from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from bs4 import BeautifulSoup
import time

class EpicMafiaBot():

    def __init__(self, username, password):

        self.browser = webdriver.Chrome('chromedriver.exe')
        self.username = username
        self.password = password


    def logIn(self):

        self.browser.get('https://epicmafia.com/home')

        usernameInput = self.browser.find_elements_by_css_selector('form input')[0]
        passwordInput = self.browser.find_elements_by_css_selector('form input')[1]

        usernameInput.send_keys(self.username)
        passwordInput.send_keys(self.password)
        passwordInput.send_keys(Keys.ENTER)

    def showGames(self):
        time.sleep(3)

        games = self.browser.find_elements_by_class_name('gamerow vv mafia_gamerow join')
        gamepage = self.browser.find_element_by_id('gamepage_inner')

        doggo = BeautifulSoup(self.browser.page_source)

        print(doggo)




